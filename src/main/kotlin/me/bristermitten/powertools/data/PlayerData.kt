package me.bristermitten.powertools.data

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import org.bukkit.Material
import java.util.*


@Serializable
data class PlayerData(
    @SerialName("power-tools")
    val powerTools: MutableMap<Material, String> = EnumMap(Material::class.java)
)
