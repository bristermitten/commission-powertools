package me.bristermitten.powertools.data

import com.charleskorn.kaml.Yaml
import org.bukkit.entity.Player
import java.io.Reader
import java.io.Writer
import java.util.*
import kotlin.collections.HashMap

object PlayerDataStore {

    private val data = PlayerDataMap(HashMap())

    fun getOrCreateData(uuid: UUID): PlayerData {
        return data.computeIfAbsent(uuid) {
            PlayerData()
        }
    }

    fun getOrCreateData(player: Player): PlayerData = getOrCreateData(player.uniqueId)

    fun get(uuid: UUID): PlayerData? = data[uuid]
    fun clear() = data.clear()

    /**
     * Save all PlayerData to a given [Writer] in YAML format
     * This will close the writer
     */
    fun saveAllTo(writer: Writer) {
        val yaml = Yaml.default
        val written = yaml.stringify(PlayerDataMap.serializer(), data)

        writer.write(written)
        writer.flush()
        writer.close()
    }

    /**
     * Load all data from a given [Reader]
     * This assumes a YAML format, and will close the reader
     */
    fun loadAllFrom(reader: Reader) {
        val yaml = Yaml.default
        val store = yaml.parse(PlayerDataMap.serializer(), reader.readText())

        this.data.clear()
        this.data.putAll(store)

        reader.close()

        println("Loaded ${store.size} Players with ${store.map { it.value.powerTools.values.size }.sum()} Power Tools!")
    }
}

