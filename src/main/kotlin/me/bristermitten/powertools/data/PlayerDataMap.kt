package me.bristermitten.powertools.data

import kotlinx.serialization.Serializable
import java.util.*

/**
 * Yes, this is hideous. But it seems to be the only way to allow kotlinx.serialization
 * to serialize the map with the required [UUIDSerializer] without a hacky mess
 */
@Serializable
class PlayerDataMap(
    private val data: MutableMap<@Serializable(with = UUIDSerializer::class) UUID, PlayerData>
) : MutableMap<UUID, PlayerData> by data
