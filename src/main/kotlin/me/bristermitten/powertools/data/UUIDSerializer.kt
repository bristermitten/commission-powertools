package me.bristermitten.powertools.data

import kotlinx.serialization.*
import kotlinx.serialization.internal.StringDescriptor
import java.util.*

@Serializer(forClass = UUID::class)
class UUIDSerializer : KSerializer<UUID> {
    override val descriptor: SerialDescriptor
        get() = StringDescriptor.withName("UUID")

    override fun serialize(encoder: Encoder, obj: UUID) {
        encoder.encodeString(obj.toString())
    }

    override fun deserialize(decoder: Decoder): UUID {
        return UUID.fromString(decoder.decodeString())
    }
}
