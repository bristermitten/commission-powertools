package me.bristermitten.powertools.commands

import co.aikar.commands.BaseCommand
import co.aikar.commands.annotation.CatchUnknown
import co.aikar.commands.annotation.CommandAlias
import co.aikar.commands.annotation.CommandPermission
import co.aikar.commands.annotation.Default
import org.bukkit.entity.Fireball
import org.bukkit.entity.Player

@CommandAlias("fireball")
@CommandPermission("powertools.fireball")
class FireballCommand : BaseCommand() {

    @Default
    @CatchUnknown
    fun perform(player: Player) {
        val direction = player.eyeLocation.direction.multiply(2)

        val fireball = player.world.spawn(player.eyeLocation.add(direction), Fireball::class.java)
        fireball.velocity = direction
        fireball.shooter = player
    }
}

@CommandAlias("lightning")
@CommandPermission("powertools.lightning")
class LightningCommand : BaseCommand() {

    @Default
    @CatchUnknown
    fun perform(player: Player) {
        player.world.strikeLightning(player.getTargetBlock(null, 600).location)
    }
}
