package me.bristermitten.powertools.commands

import co.aikar.commands.BaseCommand
import co.aikar.commands.annotation.CatchUnknown
import co.aikar.commands.annotation.CommandAlias
import co.aikar.commands.annotation.CommandPermission
import co.aikar.commands.annotation.Default
import me.bristermitten.powertools.data.PlayerDataStore
import me.bristermitten.powertools.lang.Errors
import me.bristermitten.powertools.lang.Info
import me.bristermitten.powertools.util.prettyName
import org.bukkit.Material
import org.bukkit.entity.Player

@CommandAlias("powertool")
class PowerToolCommand : BaseCommand() {

    @Default
    @CommandPermission("powertool.edit")
    fun clear(player: Player) {
        val type = player.inventory.itemInMainHand.type
        if (type.isAir) {
            currentCommandIssuer.sendError(Errors.NOT_AIR)
            return
        }

        val original = PlayerDataStore.getOrCreateData(player).powerTools.remove(type)
        if (original == null) {
            currentCommandIssuer.sendInfo(Info.NO_POWER_TOOLS, "{type}", type.prettyName())
        } else
            currentCommandIssuer.sendInfo(Info.POWER_TOOL_REMOVED, "{type}", type.prettyName())
    }

    @CommandPermission("powertool.edit")
    @CatchUnknown
    fun createPowerTool(player: Player, args: Array<String>) {
        val type = player.inventory.itemInMainHand.type
        if (type.isAir) {
            currentCommandIssuer.sendError(Errors.NOT_AIR)
            return
        }

        val command = args.joinToString(" ")
        PlayerDataStore.getOrCreateData(player).powerTools[type] = command

        currentCommandIssuer.sendInfo(Info.POWER_TOOL_CREATED, "{type}", type.prettyName(), "{command}", command)
    }
}
