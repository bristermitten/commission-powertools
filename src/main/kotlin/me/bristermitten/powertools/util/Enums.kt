package me.bristermitten.powertools.util


fun Enum<*>.prettyName() = name.toLowerCase().replace("_", " ").split(" ").joinToString(" ") { it.capitalize() }
