package me.bristermitten.powertools.lang

import co.aikar.locales.MessageKey
import co.aikar.locales.MessageKeyProvider

enum class Info : MessageKeyProvider {
    POWER_TOOL_CREATED,
    POWER_TOOL_REMOVED,
    NO_POWER_TOOLS,
    ;

    override fun getMessageKey(): MessageKey {
        return MessageKey.of("powertools.info-" + name.toLowerCase().replace('_', '-'))
    }
}
