package me.bristermitten.powertools.lang

import co.aikar.locales.MessageKey
import co.aikar.locales.MessageKeyProvider

enum class Errors : MessageKeyProvider {
    NOT_AIR;

    override fun getMessageKey(): MessageKey {
        return MessageKey.of("powertools.errors-" + name.toLowerCase().replace('_', '-'))
    }
}
