package me.bristermitten.powertools

import co.aikar.commands.PaperCommandManager
import me.bristermitten.powertools.commands.FireballCommand
import me.bristermitten.powertools.commands.LightningCommand
import me.bristermitten.powertools.commands.PowerToolCommand
import me.bristermitten.powertools.data.PlayerDataStore
import org.bukkit.Bukkit
import org.bukkit.plugin.java.JavaPlugin
import java.io.File
import java.util.*

class PowerTools : JavaPlugin() {

    private lateinit var dataFile: File

    override fun onEnable() {
        setupFiles()
        setupCommands()


        Bukkit.getPluginManager().registerEvents(PowerToolListener(), this)
    }

    private fun setupFiles() {
        saveResource("data.yml", false)
        dataFile = File(dataFolder, "data.yml")

        PlayerDataStore.loadAllFrom(dataFile.reader())

        saveResource("lang_en.yml", false)
    }

    override fun onDisable() {
        PlayerDataStore.saveAllTo(dataFile.writer())
    }

    private fun setupCommands() {
        val commandManager = PaperCommandManager(this)

        commandManager.locales.loadYamlLanguageFile("lang_en.yml", Locale.ENGLISH)
        commandManager.locales.loadLanguages()


        commandManager.registerCommand(PowerToolCommand())
        commandManager.registerCommand(FireballCommand())
        commandManager.registerCommand(LightningCommand())
    }
}
