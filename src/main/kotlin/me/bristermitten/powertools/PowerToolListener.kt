package me.bristermitten.powertools

import me.bristermitten.powertools.data.PlayerDataStore
import org.bukkit.Bukkit
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.player.PlayerInteractEvent

class PowerToolListener : Listener {

    @EventHandler
    fun onClick(e: PlayerInteractEvent) {
        if (e.action == Action.PHYSICAL) return

        val type = e.item?.type ?: return

        val command = PlayerDataStore.getOrCreateData(e.player).powerTools[type] ?: return

        Bukkit.dispatchCommand(e.player, command)

        e.isCancelled = true
    }
}
