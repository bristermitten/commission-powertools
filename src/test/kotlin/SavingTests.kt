import kotlinx.serialization.ImplicitReflectionSerializer
import me.bristermitten.powertools.data.PlayerDataStore
import me.bristermitten.powertools.util.prettyName
import org.bukkit.Material
import org.junit.Test
import java.io.StringReader
import java.io.StringWriter
import java.util.*

class SavingTests {

    @ImplicitReflectionSerializer
    @Test
    fun `Test PowerTools loading and saving to config`() {

        val uuid = UUID.randomUUID()
        val data = PlayerDataStore.getOrCreateData(uuid)

        data.powerTools[Material.STONE] = "say hi"

        val writer = StringWriter()
        PlayerDataStore.saveAllTo(writer)

        val yaml = writer.toString()
        println(yaml)

        PlayerDataStore.clear()

        PlayerDataStore.loadAllFrom(StringReader(yaml))

        println(PlayerDataStore.get(uuid))

        println(Material.TRIDENT == Material.AIR)
        println(Material.TRIDENT == Material.AIR)
    }
}
